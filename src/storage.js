'use strict'
var fs = require('fs');

function getCommentData (fileName) {
    return new Promise((resolve, reject) => {
        try {
            let data = fs.readFileSync(fileName, 'utf-8');
            jsonData = JSON.parse(data);
            resolve(jsonData)
        } catch (e) {
            reject(e);
        }
    })
}

module.exports = getCommentData;
