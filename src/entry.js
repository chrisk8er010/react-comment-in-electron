var React = require('react');
var ReactDom = require('react-dom');

var fs = window.require('fs');
var $ = jQuery = require('jquery');

var Comment = React.createClass({
    render: function() {
        return (
            <div className="comment">
                <h2 className="commentAuthor">{this.props.author}</h2>
                {this.props.children}
            </div>
        );
    }
});

var CommentList = React.createClass({
    render: function() {
        var commentNodes = this.props.data.map(function(comment) {
            return (
                <Comment author={comment.author} >
                    {comment.text}
                </Comment>
            );
        });
        return (
            <div className="commentList">
                {commentNodes}
            </div>
        );
    }
});

var CommentForm = React.createClass({
    getInitialState: function() {
        return {author: "", text: ""};
    },
    handleAuthorChange: function(e) {
        this.setState({author: e.target.value});
    },
    handleTextChange: function(e) {
        this.setState({text: e.target.value});
    },
    handleSubmit: function(e) {
        e.preventDefault();
        var author = this.state.author.trim();
        var text = this.state.text.trim();
        //cancel if author and text is empty
        if (!text || !author) {
            return
        }
        this.props.onCommentSubmit({author: author, text: text});
        //clear the textbox
        this.setState({author: '', text: ''});
    },
    render: function() {
        return (
            <form className="commentForm" onSubmit={this.handleSubmit}>
                <input type="text" value={this.state.author} placeholder="Your name here" onChange={this.handleAuthorChange} />
                <input type="text" value={this.state.text} placeholder="Say something..." onChange={this.handleTextChange} />
                <input type="submit" value="Post" />
            </form>
        );
    }
});

var CommentBox = React.createClass({
    getInitialState: function() {
        return {data: []};
    },
    getCommentFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.log(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    handleCommentSubmit: function(data) {
        jsonData = JSON.parse(fs.readFileSync('./data/Comment.json'));
        jsonData.push(data);
        fs.writeFileSync('./data/Comment.json', JSON.stringify(jsonData), 'utf-8');
    },
    componentDidMount: function() {
        setInterval(this.getCommentFromServer, 2000);
    },
    render: function() {
        return (
            <div className="commentBox">
                <h1>Comment</h1>
                <CommentList data={this.state.data} />
                <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
            </div>
        );
    }
});

ReactDom.render(<CommentBox url="./data/comment.json" />, document.getElementById('container'));
